import dbaccess as db
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import numpy as np
from dash.dependencies import Input, Output
import flask

server = flask.Flask(__name__)

df_header = ["Index", "SensorID", "Timestamp", "Latitude", "Longitude"]
mapbox_access_token = "pk.eyJ1IjoieWVudGh1bmd1eWVuIiwiYSI6ImNqdmZvNXQ5djBqYmc0ZnFwbGlicWplcHIifQ.CdvxuFW09O2tz0IE-urA4Q"

dbconn = db.connect()
sensor_data = db.get_gps_data(dbconn)
df = pd.DataFrame(sensor_data, columns = df_header)
print(df)

@server.route('/')
def index():
    return 'Hello Flask app'

app = dash.Dash(__name__, server=server, routes_pathname_prefix='/dash/')

app.layout = html.Div([
    html.Div([
        html.H1("GPS sensor locations test")
    ], style={
        'textAlign': "center",
        "padding-bottom": "10",
        "padding-top": "10"}),
    html.Div([
        html.Div([
            html.H3("Filter by sensor ID")
        ], style={
            'textAlign': "center"}),
        dcc.Dropdown(id="sensorID",
                     options=[{'label': i, 'value': i} for i in df.SensorID],
                     value=[],
                     multi=True,
                     style={
                         "display": "block",
                         "margin-left": "auto",
                         "margin-right": "auto",
                         "width": "50%"

                     }
                     )
    ]),
    html.Div(dcc.Graph(id="my-graph"))

],className="container")

@app.callback(
    Output("my-graph", "figure"),
    [Input("sensorID", "value")]

)
def update_figure(selected):
    trace = []
    for sensorID in selected:
        dff = df[df["SensorID"] == sensorID]
        trace.append(go.Scattermapbox(
            lat=dff["Latitude"],
            lon=dff["Longitude"],
            mode='markers',
            marker={'symbol':"marker", 'size': 20}, 
            text=sensorID,
            hoverinfo='text',
            name=sensorID
        ))
    return {
        "data": trace,
        "layout": go.Layout(
            autosize=True,
            hovermode='closest',
            showlegend=False,
            height=700,
            mapbox={'accesstoken': mapbox_access_token,
                    'bearing': 0,
                    'center': {'lat': 51.4, 'lon': 6.9},
                    'pitch': 5, 'zoom': 10,
                    "style": 'mapbox://styles/mapbox/light-v9'},
            uirevision="static"
        )

    }


def cast_point(value):
    if value is None:
        return None

    # Convert from (f1, f2) syntax using a regular expression.
    m = re.match(r"\(([^)]+),([^)]+)\)", value)
    if m:
        return [float(m.group(1)), float(m.group(2))]
    else:
        raise Exception("bad point representation: %r" % value)


if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0')
