
import psycopg2
from configparser import ConfigParser


db_host='postgres'
port=5432
database='datalog_db'
user='postgres'
password='postgres'

conn = None

def connect():
    """ Connect to the PostgreSQL database server """
    global conn
    try:
        # read configs from config file
        # db_params = read_config(config_file, section_name)
        
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(host=db_host, port=port, user=user, password=password, database=database)
    
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        """ if conn is not None:
            conn.close()
            print('Database connection closed.') """
        return conn


def get_gps_data(dbconn):
    res = None
    try:
        # create a cursor
        cur = dbconn.cursor()
        
        # execute a statement
        query = 'SELECT * FROM public."GPS_DATA_NEW"'
        cur.execute(query)

        # display response from DB
        res = cur.fetchall()
        #print(res)
       
        # close the communication with the DB
        cur.close()
    
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        """ if conn is not None:
            dbconn.close()
            print('Database connection closed.') """
        return res


def read_config(filename, section):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)
 
    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        db_params = parser.items(section)
        for param in db_params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))
 
    return db
 

