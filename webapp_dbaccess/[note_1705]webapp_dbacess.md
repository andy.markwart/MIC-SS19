## Date 17.05.2019
### First attemp to retrieve dummy GPS data from remote Postgres DB running inside a container and display on map
#### Scripts available at [app.py](../webapp_dbaccess/app/app.py) and [dbaccess.py](../webapp_dbaccess/app/dbaccess.py), modified from [this tutorial](http://www.postgresqltutorial.com/postgresql-python/connect/).

#### Run locally
- Create a configuration file called `database.ini` in the same folder as `app.py` and `dbaccess.py` to store confidential database access data as follows:
```bash
#content of database.ini file
[postgresql]  #section name
host=www.example.com  #IP address or name of host
port:5432 #default Postgres port
datatabase=datalog_db #name of database
user=postgres  #postgres username
password=postgres   #postgres password
```
The `dbaccess.py` script will make use of this config file to connect to the database.
- Install `psycopg2` to handle connections and interactions with database (assuming other libaries in `requirements.txt` have been installed before according to previous notes)
```bash
pip install psycopg2
```
- Run `python app.py` to launch the app. The app is available at `localhost:8050`. Screencap of example app:
<img src = "../media/app_test_1.png" height="480px">

#### Run using docker containers:
```
docker-compose up --build -d
```
The app is currently available at [www.datalog.live:5000/dash/](www.datalog.live:5000/dash/)