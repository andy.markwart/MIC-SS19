# Install Docker
All operations are performed on a Virtual Private Server (VPS) running Ubuntu 18.04 LTS unless otherwise specified.
 - create <a href="https://gitlab.com/yenthusiastic/MIC-SS19" target="_blank">GitLab repo</a>
 - add new sudo user for Andy following steps described in <a href="https://linuxize.com/post/how-to-create-a-sudo-user-on-ubuntu/" target="_blank">this site</a>
 - purchase new domain name <a href="http://datalog.live" target="_blank">datalog.live</a> and set up server to host a sample HTML page
 - install docker with following steps as described in <a href="https://www.linode.com/docs/applications/containers/deploying-microservices-with-docker/" target="_blank">this site</a>
   - install required dependencies and verify GPG key of Docker
```powershell
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
```

   - - install Docker Community Edition (CE)
```powershell
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt update
sudo apt install docker-ce
```
   - - run the built-in Hello World example with command `docker run hello-world` to verify installation:
```
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
1b930d010525: Pull complete
Digest: ****
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.
...
```
 - use <a href="https://www.linode.com/docs/applications/containers/how-to-use-dockerfiles/" target="_blank">Dockerfile</a> to configure Docker container image[^1] by creating a new file named `Dockerfile` and paste following contents:
[^1]: View best practices for writing Dockerfiles <a href="https://docs.docker.com/develop/develop-images/dockerfile_best-practices/" target="_blank">here</a>.
```powershell
 # Use an official Python runtime as a parent image
FROM python:3.7-slim

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variable
ENV NAME World

# Run app.py when the container launches
CMD ["python", "app.py"]
```
 - create `requirements.txt` and `app.py` in the same folder as `Dockerfile` as instructed in <a href="https://docs.docker.com/get-started/part2/" target="_blank">this site</a> and build an image of the app with a tag name:
```
docker build --tag=friendlyhello .
```
>  Note: When the above Dockerfile is built into an image, app.py and requirements.txt is present because of that Dockerfile’s COPY command, and the output from app.py is accessible over HTTP thanks to the EXPOSE command.
 - Run the app, mapping local machine’s port 4000 to the container’s published port 80 using -p:
```
docker run -p 4000:80 friendlyhello
```
The app is now live at <a href="http://www.datalog.live:4000/" target="_blank">www.datalog.live:4000</a>

To run the app in the background, first stop the container by `Ctrl+C` and then run command
```powershell
docker run -d -p 4000:80 friendlyhello
```
In order to stop the container, first have to find out the `CONTAINER ID` using command
```powershell
docker container ls
```
Example output:
```
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                 
123a456b789c        friendlyhello       "python app.py"     3 minutes ago       Up 3 minutes        0.0.0.0:4000->80/tcp
```
Then use the stop command to stop the container using its ID from above output
```powershell
docker container stop 123a456b789c
```



# Docker Compose and micro services
Continue Docker Getting Started tutorial from <a href="https://docs.docker.com/get-started/part2/" target="_blank">Docker Docs</a>.
### Share built Docker image to cloud for remote pulling
 - Create Docker Hub account
 - Create new <a href="https://hub.docker.com/r/yenthusiastic/get-started" target="_blank">Docker repo</a> to push the built container image
 - Added collaborator to repo
 - In terminal, log in to this account using `docker login` command
 - Upon successful login, tag the built image using `docker tag <image> <username>/<repository>:<tag>` command, specifying respective created image name, username and repository name
 - Publish the image using command 
```powershell
docker push <username>/<repository>:<tag>
```
In this note, the app is published to <a href="https://hub.docker.com/r/yenthusiastic/get-started/tags" target="_blank">yenthusiastic/get-started:friendlyhello</a>.
 - On another PC (already installed with Docker CE), pull and run the image from the remote Docker repo using command
```powershell
docker run -p 127.0.0.1:4000:80 <username>/<repository>:<tag>
``` 
 This command will automatically pull the image along with Python and all dependencies if the image is not already available on the local machine, and run the app after pulling.
The app has successfully been deployed on a PC running Ubuntu 16.10 with Docker installed and is accessible at <a href="http://127.0.0.1:4000">http://127.0.0.1:4000</a>.

`IMPORTANT NOTE:` If we dont't provide the local host IP address (*127.0.0.1*) at the `port` flag in the above command, i.e. only ` -p 4000:80` instead, the app will be accessible at <a href="http://0.0.0.0:4000">http://0.0.0.0:4000</a>. This means other computers on the same network will be able to reach it. 

### Run micro services
 - Install latest release of <a href="https://github.com/docker/compose/releases" target="_blank">Docker Compose</a> (1.24.0 as of time of writing):
```
curl -L https://github.com/docker/compose/releases/download/1.24.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
```
 - Set file permissions
```
sudo chmod +x /usr/local/bin/docker-compose
```
 - Create a `docker-compose.yml` YAML file (human-readable markup language) to define how Docker containers should behave in production.
```powershell
version: "3"
services:
  web:
    # replace username/repo:tag with your name and image details
    image: username/repo:tag
    ports:
      - "4000:80"     # Map port 4000 on the host to port 80 of 'web'.
```
 - Fire the container using command `docker-compose up --build -d`

# Test to link python app and redis store for stateful applications
 - Some useful new commands:
```powershell
docker container rm <CONTAINER ID>  # remove a container by ID
docker image rm  <IMAGE ID>   # remove an image by ID
docker system prune  # WARNING! This will remove:
                     #  - all stopped containers
                     #  - all networks not used by at least one container
                     #  - all dangling images
                     #  - all build cache
```

 - Create a new `docker-compose` file in the same folder to add links to redis 
```powershell
version: '3'  
services:  
  app:
    build: ./
    depends_on:
      - redis
    environment:
      - REDIS_HOST=redis
    ports:
      - "3000:80"
    links:
      - redis
  redis:
    image: redis
    expose:
      - 6379
    volumes:
      - redis_data:/data
volumes:  
  redis_data:
```

 - Run `docker-compose up -d`. This command builds, (re)creates, starts, and attaches to containers for a service and keeps it running in the background. Unless they are already running, this command also starts any linked services. 
 This commands creates 2 new containers called *test_app_1* and *test_redis_1* by default. The app runs on port 3000 at <a href="http://www.datalog.live:3000/" target="_blank">http://www.datalog.live:3000/</a>

### To link app container with already existing redis container
 - Start a new container running redis
```powershell
docker run -d -p 6379:6379 --name redis1 redis
```
 - Stop the app already running on port 3000 using its ID
 - In the folder containing `Dockerfile`, `docker-compose.yml`, `app.py` and `requirements.txt`, create build image for the app with a tag name
```
docker build --tag=testredis .
```
 - Create a new container from the image, give it a name and link it with the created redis container from above by its name
```powershell
docker run -d -p 3000:80 --link redis1:redis --name testredis1 testredis
```
We link it to the redis1 container (which is still running), and it will be referred to from within this container simply as redis.

### Extras
 - Run the Redis CLI in the container
```powershell
docker exec -it redis1 sh
```
 - Try out some basic Redis commands. If we send a "ping", should get "PONG" back:
```powershell
127.0.0.1:6379> ping
PONG
```
 - Try out some more commands (set a key and increment a counter)
```powershell
127.0.0.1:6379> set name mark
OK
127.0.0.1:6379> get name
"mark"
127.0.0.1:6379> incr counter
(integer) 1
127.0.0.1:6379> incr counter
(integer) 2
127.0.0.1:6379> get counter
"2"
```
 - And when we're done exit out of redis-cli and sh:
```powershell
127.0.0.1:6379> exit
# exit
```

# Installing Postgres database from docker image
Notes following tutorial from [Hackernoon](https://hackernoon.com/dont-install-postgres-docker-pull-postgres-bee20e200198).
- Pull Postgres docker image with `docker pull postgres`
- Create a volume for persistent storage beyond the container’s lifecycle with `mkdir $HOME/docker/volumes/postgres`
- Start the Postgres container using <a name="dockerrun">docker run</a> command, provide appropriate flags
```powershell
docker run --rm   --name pg-docker -e POSTGRES_PASSWORD=docker -d -p 127.0.0.1:5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data  postgres
```
Explanation of the flags used in above command:
> `--rm`: Automatically remove the container and its associated file system within the container upon exit to release disk space.
>
> `--name`: A name for the container. Note that two existing (even if they are stopped) containers cannot have the same name. To re-use a name, either pass `--rm` flag to the docker run command or explicitly remove the container by using the command `docker rm [container name]`
>
> `--e`: Expose environment variable named *POSTGRES_PASSWORD* with value *docker* to the container. This environment variable sets the superuser password for PostgreSQL. There are additional environment variables you can set like *POSTGRES_USER* and *POSTGRES_DB*. *POSTGRES_USER* sets the superuser name. If not provided, the superuser name defaults to *postgres*. *POSTGRES_DB* sets the name of the default database to setup. If not provided, it defaults to the value of *POSTGRES_USER*.
>
> `--d`: Launches the container in detached mode (in the background).
>
> `--p`: Bind port *5432* on localhost (*127.0.0.1*) to port *5432* within the container. This option enables applications running out side of the container to be able to connect to the Postgres server running inside the container. If localhost IP address *127.0.0.1* is not provided, it will run on *0.0.0.0* which means other computers on the same network will be able to reach it.
>
> `--v`: Mount $*HOME/docker/volumes/postgres* on the host machine to the container-side volume path */var/lib/postgresql/data* created inside the container. This ensures that postgres data persists even after the container is removed.

- Install the Postgres interactive terminal PSQL (for Ubuntu 14.04, 16.04, 18.04, postgres-client-10 is available). For other OS, check [this](https://www.postgresql.org/download/).
```powershell
sudo apt-get install postgres-client-10
```
- Connect to the container from another application using PSQL
```powershell
psql -h localhost -U postgres -d postgres
```
Note: 
> `-h`: hostname
>
> `-U`: username
>
> `-d`: database name

For more flags info check [this](https://www.postgresql.org/docs/current/app-psql.html).
- Enter *docker* when prompted for password, or whatever password was set in the `docker run` command [above](#dockerrun).
- Create an example table named *test_sensor* for the sensor database
```p
postgres=# CREATE TABLE test_sensor(
postgres(# id integer,
postgres(# sensor_id integer,
postgres(# sensor_desc text,
postgres(# wifi_ssid text,
postgres(# gps_loc point,
postgres(# humidity real,
postgres(# temperature real);
```
More details on data types and SQL commands, check the [documentation](https://www.postgresql.org/docs/10/index.html).
- Show the table structure of table *test_sensor*
```
postgres=# \d test_sensor
```
Result:
Table "public.test_sensor"

   Column    |  Type   | Collation | Nullable | Default
-------------|---------|-----------|----------|---------
 id          | integer |           |          |
 sensor_id   | integer |           |          |
 sensor_desc | text    |           |          |
 wifi_ssid   | text    |           |          |
 gps_loc     | point   |           |          |
 humidity    | real    |           |          |
 temperature | real    |           |          |

- Try to insert values into table
```powershell
INSERT INTO test_sensor VALUES (1, 1, 'test', '12345', '(51.500756, 6.545757)', 0.98765, 27.0);
```
- Check the inserted entry
```powershell
select * from test_sensor;
```
Result:

 id | sensor_id | sensor_desc | wifi_ssid |       gps_loc        | humidity | temperature
----|-----------|-------------|-----------|----------------------|----------|-------------
  1 |         1 | test        | 12345     | (51.500756,6.545757) |  0.98765 |          27

To quit, use command `\q`


# Test Docker Flask app with Postgres

- Cloned [this Git repo](https://github.com/mehemken/docker-flask-postgres) 
- Edit the environment variables inside `.env` file using `sudo nano .env` 
> POSTGRES_USER=superuser
>
> POSTGRES_PASSWORD=superpw
>
> POSTGRES_DB=dbname
- Edit the DB parameters inside `app/app.py`
> DBUSER = 'postgres'
>
> DBPASS = 'postgres'
>
> DBHOST = 'db'
>
> DBPORT = '5432'
>
> DBNAME = 'dbname' 
- Edit the `docker-compose.yml` by appending data volume to the `db` service in order to persist Postgres data upon destruction
```powershell
  db:
    image: postgres:10                             # use docker image of postgres tagged 10
    env_file: .env                                 # use environment variables defined in .env files
    expose:
      - 5432
    volumes:
      - ./postgres-data:/var/lib/postgresql/data   # copy data from ./postgres-data on host to container's data volume upon firing
```
- Useful commands
```powershell
docker volume ls
docker volume prune
docker-compose up --build -d
docker-compose down
docker-compose logs
```


