### Date: 10.05.2019
#### Install pgAdmin in virtual environment to manage database. Notes following instructions from [Linux Hint](https://linuxhint.com/install-pgadmin4-ubuntu/).

- Make a new virtual environment for pgAdmin, activate it and upgrade pip if necessary
```
python3.7 -m venv pgAdmin4
source pgAdmin4/bin/activate
pip install --upgrade pip
```
If the installation fails with error "command 'x86_64-linux-gnu-gcc' failed with exit status 1", run `sudo apt-get install python3.7-dev`
- Download and install pgAdmin4
```bash
wget https://ftp.postgresql.org/pub/pgadmin/pgadmin4/v4.6/pip/pgadmin4-4.6-py2.py3-none-any.whl  #check and use latest version whenever available
pip install pgadmin4-4.6-py2.py3-none-any.whl
```
- Create a script called "config_local.py" to cinfgure pgAdmin
```bash
nano lib/python3.7/site-packages/pgadmin4/config_local.py
```
- Copy the configurations to the file:
```
import os
DATA_DIR = os.path.realpath(os.path.expanduser(u'~/.pgadmin/'))
LOG_FILE = os.path.join(DATA_DIR, 'pgadmin4.log')
SQLITE_PATH = os.path.join(DATA_DIR, 'pgadmin4.db')
SESSION_DB_PATH = os.path.join(DATA_DIR, 'sessions')
STORAGE_DIR = os.path.join(DATA_DIR, 'storage')
SERVER_MODE = False
```
- Use following command to run pgAdmin
```bash
python lib/python3.7/site-packages/pgadmin4/pgAdmin4.py
```
Default hosting server is '127.0.0.1'. In order to host over LAN, edit the `config.py` file
```bash
nano lib/python3.7/site-packages/pgadmin4/config.py
```
and set `DEFAULT_SERVER = '0.0.0.0'` 