## 02.05.2019

### Test Docker Flask app with Postgres

- Cloned [this Git repo](https://github.com/mehemken/docker-flask-postgres) 
- Edit the environmnet variables inside `.env` file using `sudo nano .env` 
> POSTGRES_USER=superuser
>
> POSTGRES_PASSWORD=superpw
>
> POSTGRES_DB=dbname
- Edit the DB parameters inside `app/app.py`
> DBUSER = 'postgres'
>
> DBPASS = 'postgres'
>
> DBHOST = 'db'
>
> DBPORT = '5432'
>
> DBNAME = 'dbname' 
- Edit the `docker-compose.yml` by appending data volume to the db service in order to persist Postgres data upon destruction
```
  db:
    image: postgres:10                             # use docker image of postgres tagged 10
    env_file: .env                                 # use environment variables defined in .env files
    expose:
      - 5432
    volumes:
      - ./postgres-data:/var/lib/postgresql/data   # copy data from ./postgres-data on host to container's data volume upon firing
```
- Useful commands
```
docker volume ls
docker volume prune
docker-compose up --build -d
docker-compose down
docker-compose logs
```