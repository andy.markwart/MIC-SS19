### 08.04.19
All operations are performed on a Virtual Private Server (VPS) running Ubuntu 18.04 LTS unless otherwise specified.
 - create <a href="https://gitlab.com/yenthusiastic/MIC-SS19" target="_blank">GitLab repo</a>
 - add new sudo user for Andy following steps described in <a href="https://linuxize.com/post/how-to-create-a-sudo-user-on-ubuntu/" target="_blank">this site</a>
 - purchase new domain name <a href="http://datalog.live" target="_blank">datalog.live</a> and set up server to host a sample HTML page
 - install docker with following steps as described in <a href="https://www.linode.com/docs/applications/containers/deploying-microservices-with-docker/" target="_blank">this site</a>
   - install required dependencies and verify GPG key of Docker
```powershell
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
```

   - - install Docker Community Edition (CE)
```powershell
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt update
sudo apt install docker-ce
```
   - - run the built-in Hello World example with command `docker run hello-world` to verify installation:
```
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
1b930d010525: Pull complete
Digest: ****
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.
...
```
 - use <a href="https://www.linode.com/docs/applications/containers/how-to-use-dockerfiles/" target="_blank">Dockerfile</a> to configure Docker container image[^1] by creating a new file named `Dockerfile` and paste following contents:
[^1]: View best practices for writing Dockerfiles <a href="https://docs.docker.com/develop/develop-images/dockerfile_best-practices/" target="_blank">here</a>.
```powershell
 # Use an official Python runtime as a parent image
FROM python:3.7-slim

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variable
ENV NAME World

# Run app.py when the container launches
CMD ["python", "app.py"]
```
 - create `requirements.txt` and `app.py` in the same folder as `Dockerfile` as instructed in <a href="https://docs.docker.com/get-started/part2/" target="_blank">this site</a> and build an image of the app with a tag name:
```
docker build --tag=friendlyhello .
```
>  Note: When the above Dockerfile is built into an image, app.py and requirements.txt is present because of that Dockerfile’s COPY command, and the output from app.py is accessible over HTTP thanks to the EXPOSE command.
 - Run the app, mapping local machine’s port 4000 to the container’s published port 80 using -p:
```
docker run -p 4000:80 friendlyhello
```
The app is now live at <a href="http://www.datalog.live:4000/" target="_blank">www.datalog.live:4000</a>

To run the app in the background, first stop the container by `Ctrl+C` and then run command
```powershell
docker run -d -p 4000:80 friendlyhello
```
In order to stop the container, first have to find out the `CONTAINER ID` using command
```powershell
docker container ls
```
Example output:
```
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                 
123a456b789c        friendlyhello       "python app.py"     3 minutes ago       Up 3 minutes        0.0.0.0:4000->80/tcp
```
Then use the stop command to stop the container using its ID from above output
```powershell
docker container stop 123a456b789c
```