## Virtual environment (venv) for JupyterLab
The venv enables a defined default environment and kernel for JupyterLab. 

Make and activate the venv named *jupyterlab*, using Python 3.7.
```bash
python3.7 -m venv jupyterlab
source jupyterlab/bin/activate
```


## Install
```bash
pip install jupyterlab
```

### Make IPyKernel
Creating an IPython kernels makes a venv accessible for JupyterLab. 
The kernel can be chosen in the drop-down menu (top right) in a notebook.

Make kernel **pyOTA** with displayed name (drop-down menu) **3.6_pyOTA**
```bash
python -m ipykernel install --user --name pyOTA --display-name 3.6_pyOTA
```
### Start JupyterLab
Go to the directory, that should be the default directory for JupyterLab. 
> Run JupyterLab public on port 8080.
```bash
jupyter lab --ip 0.0.0.0 --port 8080
```

## Sources
[Kernel Install](https://ipython.readthedocs.io/en/latest/install/kernel_install.html) 

[JupyterHub](https://jupyterlab.readthedocs.io/en/stable/user/jupyterhub.html)