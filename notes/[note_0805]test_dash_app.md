## 08.05.2019
### First attempt to use Plotly and Dash for displaying location data on a map as seen in [this example](https://dash-simple-apps.plotly.host/dash-scattermapboxplot/).
#### Script available at [example_map.py](../SimpleDashApp/example_map.py), modified from [this source code](https://github.com/plotly/simple-example-chart-apps/blob/master/scattermapbox/app.py).

- Install dash and dependencies
See note about Pyhon venv to first create a project virtual environment.
```powershell
pip install dash==0.42.0
pip install dash-daq==0.1.0
```
- Install pandas for data handling
```powershell
pip install pandas
```
- Fork `app.py` script and run in terminal
```powershell
python app.py
```
