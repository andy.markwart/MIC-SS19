from http.server import BaseHTTPRequestHandler, HTTPServer 
import json

ADDR = "0.0.0.0" #0.0.0.0 for public
PORT = 5560

class ReqHandler(BaseHTTPRequestHandler):
    def _set_headers(self, status_code):
        self.send_response(status_code)
        #self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        self.wfile.write(b'<html><body><h1>hi!</h1></body></html>')
        print(">>>Request: \n{0}".format(self.requestline))
        print(">>>Headers: \n{0}".format(self.headers))

    def do_HEAD(self):
        self._set_headers(200)
        
    def do_POST(self):
        datalen = int(self.headers['Content-Length'])
        data = self.rfile.read(datalen)
        req_json = json.loads(data)
        print(">>>Request: \n{0}".format(self.requestline))
        print(">>>Headers: \n{0}".format(self.headers))
        print(">>>JSON: \n{0}\n".format(req_json))        
        self._set_headers(200)

try:
    server = HTTPServer((ADDR, PORT), ReqHandler)
    print("serving at port", PORT)
    #server.serve_forever()
    server.handle_request()
    server.socket.close()
except KeyboardInterrupt:
    print('Server shutdown...')
    server.shutdown()