import requests as req
import urllib3
import json

api_token = "SECRET_TOKEN"
jdata = json.dumps({"msg": [ {"sensorID": '791', "data": '15.5'},{"sensorID": '221', "data": '22.0'}]})
headers = {'Content-Type': 'application/json',
           'Authorization': api_token}
r = req.post('http://req.iota.pw:5560', headers=headers, json=jdata)
print(r.status_code)